import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { LoginRoutingModule } from './login-routing.module';

/**
 * Module d'authentification
 */
@NgModule({
  imports: [
    CommonModule, FormsModule,
    TranslateModule,
    LoginRoutingModule
  ],
  providers: [],
  declarations: [],
  bootstrap: []
})
export class LoginModule { }
