import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from '../../views/register/register.component';

const routes2: Routes = [
  { path: 'signup', component: RegisterComponent }
];

/**
 * Routage du module d'authentification
 */
@NgModule({
  imports: [RouterModule.forChild(routes2)],
  exports: [RouterModule]
})
export class LoginRoutingModule {}

