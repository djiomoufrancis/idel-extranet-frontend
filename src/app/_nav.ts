interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Mon Dossier'
  },
  {
    name: 'Modifier',
    url: '#',
    icon: 'icon-pencil'
  },
  {
    name: 'Licences',
    url: '#',
    icon: 'icon-cursor'
  },
  {
    title: true,
    name: 'Depot Legal'
  },
  {
    name: 'Nouveau Depot',
    url: '#',
    icon: 'icon-pie-chart'
  },
  {
    name: 'Consulter',
    url: '#',
    icon: 'icon-bell'
  },
  {
    name: 'Reclamations',
    url: '#',
    icon: 'icon-calculator'
  },
  {
    title: true,
    name: 'ISBN'
  },
  {
    name: 'Nouvelle demande',
    url: '#',
    icon: 'icon-star'
  },
  {
    name: 'Consulter',
    url: '#',
    icon: 'icon-ban'
  },
  {
    name: 'Additionnelles',
    url: '#',
    icon: 'icon-user-follow'
  },
  {
    title: true,
    name: 'Publication Numerique'
  },
  {
    name: 'PN Libre',
    url: '#',
    icon: 'icon-cloud-download'
  },
  {
    name: 'PN Commerciale',
    url: '#',
    icon: 'icon-calculator'
  },
  {
    name: 'Participations',
    url: '#',
    icon: 'icon-people'
  },
  {
    name: 'Consulter',
    url: '#',
    icon: 'icon-layers'
  }
];
