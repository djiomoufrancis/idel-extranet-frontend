import { Injectable } from '@angular/core';
import { GenericInterface } from '../interfaces/generic-interface';
import { State } from '../../models/state';
import { Country } from '../../models/country';
import { GenericDaoService } from '../dao/generic-dao.service';
import { City } from '../../models/city';
/**
 * Implementation du Service de fourniture des donnees generiques de l'application
 */
@Injectable({
  providedIn: 'root'
})
export class GenericManagerService implements GenericInterface {

  errorMsg: string;

  constructor(private genericDAO: GenericDaoService) { }

  /**
   * Fournit la liste des Villes du Canada
   */
  loadCities(): City[] {
    const cities: City[] = []; this.errorMsg = null;
    this.genericDAO.loadCitiesJSONFile().subscribe((data: any) => {
      for (let i = 0; i < data.length; i++) {
        cities.push(data[i]);
      }
    }
      , (error: any) => this.errorMsg = error);
    if (this.errorMsg !== null || this.errorMsg !== undefined) { throw new Error(this.errorMsg); }
    return cities;
  }
  /**
   * Fournit la liste des pays
   */
  loadCountries(): Country[] {
    const countries: Country[] = []; this.errorMsg = null;
    this.genericDAO.loadCountriesJSONFile().subscribe((data: any) => {
      for (let i = 0; i < data.length; i++) {
        countries.push(data[i]);
      }
    }, (error: any) => this.errorMsg = error);
    if (this.errorMsg !== null || this.errorMsg !== undefined) { throw new Error(this.errorMsg); }
    return countries;
  }

  /**
   * Fournit la liste des provinces et Territoires
   */
  loadStates(): State[] {
    const states: State[] = []; this.errorMsg = null;
    this.genericDAO.loadStatesJSONFile().subscribe((data: any) => {
      for (let i = 0; i < data.length; i++) { states.push(data[i]); }
    }, (error: any) => this.errorMsg = error);
    if (this.errorMsg !== null || this.errorMsg !== undefined) { throw new Error(this.errorMsg); }
    return states;
  }


}
