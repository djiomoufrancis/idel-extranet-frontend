import { Injectable } from '@angular/core';
import { LoginInterface } from '../interfaces/login-interface';
import { ResetPwdRequest } from '../../models/resetPwd-request';
import { Editor } from '../../data/editor';
import { LoginRequest } from '../../models/login-request';
import { LoginDaoService } from '../dao/login-dao.service';
import { Router } from '@angular/router';
import { ApiResponse } from '../../models/api-response';

/**
 * Implementation du Service de gestion de l'authentification
 */
@Injectable({
  providedIn: 'root'
})
export class LoginManagerService implements LoginInterface {

  errorMsg: string;
  response: ApiResponse;
  public USER_EDITOR: Editor = null;


  /**
   * Constructeur d'Initialisation
   * @param loginDAO Service DAO de gestion de l'authentification
   * @param router Service de Routage
   */
  constructor(private loginDAO: LoginDaoService, private router: Router) { }

  /**
   * Authentification d'un utilisateur
   */
  login(request: LoginRequest): Editor {
    this.loginDAO.callLoginAPI(request).subscribe(
      (response) => {
        this.response = response;
        this.USER_EDITOR = this.response != null && this.response.success ? (this.response.data as Editor) : null;
      },
      (error) => {
        this.response = null;
        console.log(request);
        console.log(error);
        // throw new Error(error);
      },
    );

    return this.USER_EDITOR;
  }

  /**
   * Inscription d'un utilisateur
   */
  register(request: Editor): Editor {
    throw new Error('Method not implemented.');
  }

  /**
   * Reinitialisation du not de passe d'un utilisateur
   */
  resetPassword(request: ResetPwdRequest): void {
    throw new Error('Method not implemented.');
  }

  /**
   * Deconnexion a l'application
   */
  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('uid');
    this.loginDAO.logout();
  }

}
