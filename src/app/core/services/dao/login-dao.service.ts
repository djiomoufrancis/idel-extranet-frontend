import { Injectable } from '@angular/core';
import { LoginRequest } from '../../models/login-request';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { buildHttpHeaders, handleHttpException, callRestAPI } from '../../shared/utils/helper';
import { Editor } from '../../data/editor';
import { ApiResponse } from '../../models/api-response';
import * as cfg from '../../../../assets/config.json';


/**
 * DAO du service de gestion de l'authentification
 */
@Injectable({
  providedIn: 'root'
})
export class LoginDaoService {

  /**
   * URL de base des APIs backend
   */
  apiBaseUrl = cfg.backOfficeUrlApi + '/ext/login';

  /**
   * Injection du fournisseur http dans le constructeur
   * @param http Fournisseur Client Http
   */
  constructor(private http: HttpClient) { }

  /**
   * Appel du webservice d'auhentification
   */
  callLoginAPI(request: LoginRequest): Observable<ApiResponse> {
    return this.http.post<ApiResponse>( this.apiBaseUrl + '/signin', request, { headers: buildHttpHeaders() } )
                    .catch( error => handleHttpException(error) );
    // return callRestAPI(this.http, this.apiBaseUrl + '/signin', 'post', request);
  }

  /**
   * Appel du webservice de souscription
   */
  callRegisterAPI(request: Editor): Observable<ApiResponse> {
    return callRestAPI(this.http, this.apiBaseUrl + '/signup', 'post', request);
  }

  /**
   * Appel du webservice de recherche de la liste des Editeurs
   */
  callListEditorsAPI(page: number, size: number): Observable<ApiResponse> {
    return callRestAPI(this.http, this.apiBaseUrl + '/lister?page=' + page + '&size=' + size, 'get');
  }

  /**
   * Appel du webservice de recherche d'un editeur
   */
  callFindEditorAPI(id: string): Observable<ApiResponse> {
    return callRestAPI(this.http, this.apiBaseUrl + '/' + id, 'get');
  }

  /**
   * Recupere l'identifiant utilisateur laisse par l'authentification Shibboleth
   */
  getUid(): Observable<string> {
    return this.http.get( cfg.extranetUrlApi + '/getUid', { headers: buildHttpHeaders(), responseType: 'text' } )
                    .catch( error => handleHttpException(error) );
  }

  /**
   * Supprime l'identifiant utilisateur laisse par l'authentification Shibboleth
   */
  logout() {
    this.http.post( cfg.extranetUrlApi + '/logout', { headers: buildHttpHeaders()} ).catch( error => handleHttpException(error) );
    this.http.post( cfg.logoutUrl, { headers: new HttpHeaders().set('Content-Type', '*/*' )} )
             .catch( error => handleHttpException(error) );
  }

}
