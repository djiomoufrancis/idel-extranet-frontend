import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../../models/country';
import { City } from '../../models/city';
import { State } from '../../models/state';

/**
 * DAO du service de fourniture des donnees Generiques de l'application
 */
@Injectable({
  providedIn: 'root'
})
export class GenericDaoService {

  /**
   * Injection du Fournisseur Http dans le constructeur
   * @param http Fournisseur Client Http
   */
  constructor(private http: HttpClient) { }

  /**
   * Retourne la liste des villes
   */
  loadCitiesJSONFile(): Observable<Array<City>> {
    return this.http.get<City[]>('/idel-v2/extranet/assets/ca-cities.json');
  }

  /**
   * Retourne la liste des Pays
   */
  loadCountriesJSONFile(): Observable<Array<Country>> {
    return this.http.get<Country[]>('/idel-v2/extranet/assets/countries.json');
  }

  /**
   * Retourne la liste des Provinces
   */
  loadStatesJSONFile(): Observable<Array<State>> {
    return this.http.get<State[]>('/idel-v2/extranet/assets/states.json');
  }

}
