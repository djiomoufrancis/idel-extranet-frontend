import { Injectable } from '@angular/core';
import * as cfg from '../../../../assets/config.json';
import { HttpClient } from '@angular/common/http';
import { buildHttpHeaders, handleHttpException, getUid } from '../../shared/utils/helper.js';
import { ApiResponse } from '../../models/api-response.js';
import { Observable } from 'rxjs';
import { Editor } from '../../data/editor.js';

@Injectable({
  providedIn: 'root'
})
export class EditorDaoService {


  /**
   * URL de base des APIs backend
   */
  apiBaseUrl = cfg.backOfficeUrlApi + '/editor';

  /**
   * Injection du fournisseur http dans le constructeur
   * @param http Fournisseur Client Http
   */
  constructor(private http: HttpClient) { }

  /**
   * Modification des corrdonnees d'un editeur
   */
  updateEditor(editor: Editor): Observable<ApiResponse> {
    return this.http.post<ApiResponse>( this.apiBaseUrl + '/update', editor, { headers: buildHttpHeaders() } )
                    .catch( error => handleHttpException(error) );
  }

  /**
   * Modification des corrdonnees d'un editeur
   */
  isUpdateRequestPending(uid: string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>( this.apiBaseUrl + '/isUpdateRequestPending/' + uid, { headers: buildHttpHeaders() } )
                    .catch( error => handleHttpException(error) );
  }

}
