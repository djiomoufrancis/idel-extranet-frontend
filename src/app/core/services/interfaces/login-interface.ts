import { LoginRequest } from '../../models/login-request';
import { Editor } from '../../data/editor';
import { User } from '../../data/user';
import { ResetPwdRequest } from '../../models/resetPwd-request';

/**
 * Interface du module d'authentification
 */
export interface LoginInterface {

  /**
   * Service d'authentification
   * @param request LoginRequest (username and password)
   * @returns a User
   */
  login(request: LoginRequest): User;

  /**
   * Service d'inscription
   * @param request Editor
   */
  register(request: Editor): Editor;

  /**
   * Service de reinitialisation de mot de passe
   * @param request ResetPwdRequest
   */
  resetPassword(request: ResetPwdRequest): void;

  /**
   * Service de Deconnexion
   */
  logout(): void;
}
