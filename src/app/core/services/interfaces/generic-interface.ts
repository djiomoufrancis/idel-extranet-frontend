import { City } from '../../models/city';
import { State } from '../../models/state';
import { Country } from '../../models/country';

/**
 * Interface du service de fourniture des donnees generiques de l'application
 */
export interface GenericInterface {

  loadCities(): Array<City>;
  loadCountries(): Array<Country>;
  loadStates(): Array<State>;

}
