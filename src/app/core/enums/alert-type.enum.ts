/**
 * Enumeration des Types de notifications
 */
export enum AlertType {
  'info', 'error', 'warning', 'confirm', 'success'
}
