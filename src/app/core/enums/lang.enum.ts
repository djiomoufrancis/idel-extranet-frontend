/**
 * Enumeration des langues disponible (pour la gestion de l'internationalisation)
 */
export enum Lang {
  'fr', 'en'
}
