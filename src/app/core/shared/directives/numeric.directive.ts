import { Directive, ElementRef, HostListener, Input } from '@angular/core';

/**
 * Directive permettant d'empecher la saisie d'autres caracteres que des caracteres numeriques dans une zone de texte
 */
@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[app-numeric]'
})
export class NumericDirective {

    @Input('numericType') numericType: string; // number | decimal

    private regex: ISpecialKey = {
        number: new RegExp(/^\d+$/),
        decimal: new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g)
    };

    private specialKeys: ISpecialKey = {
        number: ['Backspace', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight'],
        decimal: ['Backspace', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight']
    };

    constructor(private el: ElementRef) {
    }

    @HostListener('keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {

        if (typeof this.specialKeys[this.numericType] === 'string' &&
            (this.specialKeys[this.numericType].toString().indexOf(event.key) !== -1)) {
            return;
        }
        // Do not use event.keycode this is deprecated.
        // See: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode
        const current: string = this.el.nativeElement.value;
        const next: string = current.concat(event.key);
        if (next && !String(next).match(this.regex[this.numericType].toString())) {
            event.preventDefault();
        }
    }

}

interface ISpecialKey {
    number: string[] | RegExp;
    decimal: string[] | RegExp;
    [data: string]: string[] | RegExp;

}
