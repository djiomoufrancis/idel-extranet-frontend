import { HttpErrorResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Alert } from '../../models/alert';
import { AlertType } from '../../enums/alert-type.enum';
import { ApiResponse } from '../../models/api-response';
import { Editor } from '../../data/editor';

export const DEFAULT_UID = '0';

/**
 * Checks if user has logged in to the app
 */
export function isLoggedIn(): boolean {
  const uid = localStorage.getItem('uid');
  return uid != null && uid !== undefined && uid !== DEFAULT_UID;
}

/**
 * Gets the uid in the localstorage
 */
export function getUid(): string {
  return isLoggedIn() ? localStorage.getItem('uid') : null;
}

/**
 * Throw all http errors
 */
export function handleHttpException(error: HttpErrorResponse) {
  console.log('Here is the HttpErrorResponse: ', JSON.stringify(error) );
  return  Observable.throwError(error.statusText + ' - ' +  error.message);
}

/**
 * Build Headers for Http Requests
 */
export function buildHttpHeaders(): HttpHeaders {
  return new HttpHeaders().set('Content-Type', 'application/json' );
                          /*.set('Accept-Charset', 'UTF-8' )
                          .set('Authorization', localStorage.getItem('token') || '21232f297a57a5a743894a0e4a801fc3' );*/
}

/**
 * Generic API Call method
 * @param http Http Client
 * @param url Webservice url
 * @param method Method
 * @param request request
 * @param params Parameters
 */
export function callRestAPI(http: HttpClient, url: string, method: string, request?: any, params?: any): Observable<any> {
  switch (method) {
    case 'post': return http.post<any>( url, request, {headers: buildHttpHeaders()} ).catch( error => handleHttpException(error) ); break;
    case 'get': return http.get<any>( url, {headers: buildHttpHeaders()} ).catch( error => handleHttpException(error) ); break;
    case 'delete': return http.delete<any>( url, {headers: buildHttpHeaders()} ).catch( error => handleHttpException(error) ); break;
    case 'put': return http.put<any>( url, request, {headers: buildHttpHeaders()} ).catch( error => handleHttpException(error) ); break;
  }
}

/**
 * Threat an display Exceptions to the user
 * @param error Error message
 */
export function threatException(error: string): Alert {
  const a = new Alert(error);
  a.setType(AlertType.error);
  a.show();
  return a;
}

