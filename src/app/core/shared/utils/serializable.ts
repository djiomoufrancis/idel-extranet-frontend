/**
 * De-Serialiateur:
 * Classe permettant de transformer toute chaine de caractere representant un objet JSon en objet TypeScript
 */
export class Serializable {
  fillFromJSON(json: string) {
    const jsonObj = JSON.parse(json);
    for (let propName in jsonObj) {
      if (propName) {
        propName = jsonObj[propName];
      }
    }
    return this;
  }
}
