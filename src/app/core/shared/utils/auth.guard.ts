import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { isLoggedIn, DEFAULT_UID } from './helper';
import { LoginDaoService } from '../../services/dao/login-dao.service';
import { LoginManagerService } from '../../services/business/login-manager.service';
import { LoginRequest } from '../../models/login-request';

/**
 * Implementation d'un garde routier afin d'empecher d'acceder a la page d'accueil de l'application sans etre authentifie
 */
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private loginService: LoginDaoService, private login: LoginManagerService) {}

    canActivate() {
      if ( !isLoggedIn() ) {
        this.loginService.getUid().subscribe(
          (response) => {
            if (response != null && response !== undefined) {
              localStorage.setItem('uid', response.toString());
              this.login.login( new LoginRequest( response.toString(), ' ' ) );
            }
          },
          (error) => {
            localStorage.setItem('uid', DEFAULT_UID);
            console.log(error);
          }
        );
      }
      return true;
        /*
        if ( isLoggedIn() ) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
        */
    }
}
