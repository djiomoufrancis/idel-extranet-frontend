/**
 * Classe representant les provinces du Canada
 */
export class State {
  constructor(public name: string, public abbreviation: string) {}
}
