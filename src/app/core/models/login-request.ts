/**
 * Modele de donnees d'une requete de demande d'authentification
 */
export class LoginRequest {
  constructor(public username: string, public password: string) {}
}
