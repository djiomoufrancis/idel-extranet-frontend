/**
 * Modele de donnees d'une reponse a une demande d'authentification
 */
export class LoginResponse {
  public token: string;

  constructor() {}
}
