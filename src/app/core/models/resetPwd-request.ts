/**
 * Modele de donnee d'une requete de demande de reinitialisation de mot de passe
 */
export class ResetPwdRequest {
  constructor(public username: string, public email: string) {}
}
