import { AlertType } from '../enums/alert-type.enum';

/**
 * Classe representant les notifications a afficher aux utilisateurs
 */
export class Alert {

  /**
   * Constructeur permettant d'initialiser une notification
   * @param msg Message
   * @param title Titre de la boite de dialogue
   * @param type Type de notification
   * @param display Visible/non
   */
  constructor(public msg: string, public title: string = 'info',
              public type: AlertType = AlertType.info,
              public display: boolean = false) {}

  setType(type: AlertType) {
    this.type = type;
    this.title = AlertType[this.type].toString().toUpperCase();
  }

  /**
   * Affiche une notification a l'utilisateur
   * @param type Type de notification
   * @param msg Message de la notification
   */
  displayAlert(type: AlertType, msg: string) {
    this.msg = msg;
    this.setType(type);
    this.show();
  }

  /**
   * Afiche la notification
   */
  show() {
    this.display = true;
  }

  /**
   * Ferme la notification
   */
  hide() {
    this.display = false;
  }
}
