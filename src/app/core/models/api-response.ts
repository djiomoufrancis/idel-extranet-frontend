export class ApiResponse {
  constructor(public code: number, public success: boolean, public message: string, public data: any) {}
}
