/**
 * Classe representant les Pays
 */
export class Country {
  constructor(public name: string, public code: string) {}
}
