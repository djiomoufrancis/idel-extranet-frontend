/**
 * Classe representant les villes du Canada
 */
export class City {
    constructor(public city: string, public admin: string, public country: string,
                public population_proper: string, public iso2: string, public capital: string,
                public lat: string, public lng: string, public population: string) {}
}
