import { Editor } from '../data/editor';

/**
 * Modele de donnees d'une requete de demande d'inscription
 */
export class SignupRequest {
  constructor(public data: Editor) {}
}
