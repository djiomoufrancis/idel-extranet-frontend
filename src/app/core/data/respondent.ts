/**
 * Classe representant un Repondant
 */
export class Respondent {

  public idRepondant: string;
  public titre: string;
  public nom: string;
  public prenom: string;
  public courriel: string;
  public telephone: string;
  public poste: string;
  public repondantRefExterne: string;
  public principal: boolean;
  public dupliquer: boolean;

  /**
   * Constructeur par defaut permettant d'initialiser un Repondant
   */
  constructor() {
    this.idRepondant = null;
    this.titre = 'M.';
    this.nom = '';
    this.prenom = '';
    this.courriel = '';
    this.telephone = '';
    this.poste = '';
    this.repondantRefExterne = null;
    this.principal = false;
    this.dupliquer = false;
  }

}
