import { LigneReclamation } from './ligne-reclamation';

export class Reclamation {

  public idReclamation: number = null;
  public nombreExemplaire: number;
  public totalExemplaire: number;
  public noteSpecifique: string = '';
  public delaiPremiereRec: number;
  public delaiDexiemeRec: number;
  public delaiTroisiemeRec: number;
  public delaiLettreRec: number;
  public delaiPremierTel: number;
  public delaiDeuxiemeTel: number;
  public delaiDernierAvis: number;
  public delaiServiceJuridique: number;
  public active: boolean = false;
  public ligneReclamations: LigneReclamation[] = [];

  constructor() {}

}
