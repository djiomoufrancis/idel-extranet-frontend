/**
 * Classe decrivant la langue d'un' editeur
 */
export class Lang {
    constructor(public idLangue?: number, public code?: string, public description?: string ) {
      this.idLangue = 0;
      this.code = '0';
      this.description = 'Français';
    }
}
