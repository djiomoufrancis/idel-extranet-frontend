import { NiveauReclamation } from './niveau-reclamation';

export class LigneReclamation {

  public idLigneReclamation: number;
  public niveauReclamation: NiveauReclamation;
  public active: boolean;
  public nombreExemplaire: number;
  public dateReponse: number;
  public noteAdditionnelle: string;

  constructor() {
    this.idLigneReclamation = null;
    this.niveauReclamation = new NiveauReclamation();
    this.active = false;
    this.nombreExemplaire = 0;
    this.dateReponse = null;
    this.noteAdditionnelle = '';
  }
}
