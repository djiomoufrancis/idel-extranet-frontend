import { Respondent } from './respondent';
import { Lang } from './lang';
import { Serializable } from '../shared/utils/serializable';

/**
 * Classe representant un Editeur
 */
export class Editor extends Serializable {

    public idEditeur: string;
    public langue: Lang;
    public editeurRefExterne: string;
    public idEditeurAssocie: string;
    public code: string;
    public nom: string;
    public adresse1: string;
    public adresse2: string;
    public ville: string;
    public provinceTerritoire: string;
    public pays: string;
    public codePostal: string;
    public telephone: string;
    public poste: string;
    public telecopieur: string;
    public courriel: string;
    public siteInternet: string;
    public commentaires: string;
    public idCompteAcces: string;
    public editeurQuebecois: string;
    public editeurActif: string;
    public paraitBottin: string;
    public estEditeur: string;
    public principal: string;
    public confidentialPhone: boolean;
    public extranetPourPublicationAccesLibre: boolean;
    public extranetPourPublicationAccesRestreint: boolean;
    public repondants: Array<Respondent>;

    /**
     * Constructeur par defaut permettant d'initialiser un editeur
     */
    constructor() {
      super();
      this.idEditeur = null;
      this.langue = new Lang();
      this.idCompteAcces = '';
      this.editeurRefExterne = null;
      this.idEditeurAssocie = null;
      this.code = '';
      this.nom = '';
      this.adresse1 = '';
      this.adresse2 = '';
      this.ville = '';
      this.provinceTerritoire = 'Québec';
      this.pays = 'Canada';
      this.codePostal = '';
      this.telephone = '';
      this.poste = '';
      this.telecopieur = '';
      this.courriel = '';
      this.siteInternet = '';
      this.commentaires = '';
      this.editeurQuebecois = 'O';
      this.editeurActif = 'O';
      this.paraitBottin = 'N';
      this.estEditeur = 'O';
      this.principal = 'N';
      this.confidentialPhone = false;
      this.extranetPourPublicationAccesLibre = false;
      this.extranetPourPublicationAccesRestreint = false;
      this.repondants = new Array<Respondent>();
    }

}

