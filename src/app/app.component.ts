import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { LoginManagerService } from './core/services/business/login-manager.service';
import { LoginRequest } from './core/models/login-request';
import { getUid } from './core/shared/utils/helper';


/**
 * Composant principal
 */
@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: `
  <router-outlet></router-outlet>
  <app-waiting-box></app-waiting-box>
`
})
export class AppComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
