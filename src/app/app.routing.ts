import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { AuthGuard } from './core/shared/utils/auth.guard';
import { HomeComponent } from './views/home/home.component';
import { DepolComponent } from './views/depol/depol.component';
import { DashboardComponent } from './views/home/dashboard/dashboard.component';
import { DossierComponent } from './views/dossier/dossier.component';
import { LicenceComponent } from './views/licence/licence.component';
import { ClaimComponent } from './views/claim/claim.component';
import { ListDepotComponent } from './views/depol/list-depot/list-depot.component';
import { ListIsbnComponent } from './views/list-isbn/list-isbn.component';
import { NewIsbnComponent } from './views/new-isbn/new-isbn.component';
import { DmdIsbnComponent } from './views/dmd-isbn/dmd-isbn.component';
import { JoinUsComponent } from './views/home/join-us/join-us.component';
import { HelpComponent } from './views/home/help/help.component';
import { EditLicenceComponent } from './views/licence/edit-licence/edit-licence.component';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home/dashboard',
    pathMatch: 'full',
  },
    {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'depol',
        component: DepolComponent,
        data: {
          title: 'Depot Legal'
        },
      },
      {
        path: 'list-depot',
        component: ListDepotComponent,
        data: {
          title: 'Liste des Depots'
        },
      },
      {
        path: 'licences',
        component: LicenceComponent
      },
      {
        path: 'dossier',
        component: DossierComponent
      },
      {
        path: 'claim',
        component: ClaimComponent
      },
      {
        path: 'dmd-isbn',
        component: DmdIsbnComponent
      },
      {
        path: 'new-isbn',
        component: NewIsbnComponent
      },
      {
        path: 'list-isbn',
        component: ListIsbnComponent
      },
      {
        path: 'join-us',
        component: JoinUsComponent
      },
      {
        path: 'help',
        component: HelpComponent
      },
      {
        path: 'edit-licence',
        component: EditLicenceComponent
      },

    ]
    },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  { path: '**', component: P404Component }
];

/**
 * Routage principal
 */
@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: false}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
