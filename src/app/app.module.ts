import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { AppRoutingModule } from './app.routing';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { GenericDaoService } from './core/services/dao/generic-dao.service';
import { GenericManagerService } from './core/services/business/generic-manager.service';
import { LoginDaoService } from './core/services/dao/login-dao.service';
import { LoginManagerService } from './core/services/business/login-manager.service';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { AlertDialogComponent } from './views/alert-dialog/alert-dialog.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AuthGuard } from './core/shared/utils/auth.guard';
import { HomeComponent } from './views/home/home.component';
import { NumericDirective } from './core/shared/directives/numeric.directive';
import { MatSliderModule } from '@angular/material/slider';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatMenuModule} from '@angular/material/menu';
import { DepolComponent } from './views/depol/depol.component';
import { HeaderComponent } from './views/home/header/header.component';
import { FooterComponent } from './views/home/footer/footer.component';
import { DashboardComponent } from './views/home/dashboard/dashboard.component';
import { DossierComponent } from './views/dossier/dossier.component';
import { LicenceComponent } from './views/licence/licence.component';
import { ClaimComponent } from './views/claim/claim.component';
import { ListDepotComponent } from './views/depol/list-depot/list-depot.component';
import { DataTablesModule } from 'angular-datatables';
import { DmdIsbnComponent } from './views/dmd-isbn/dmd-isbn.component';
import { NewIsbnComponent } from './views/new-isbn/new-isbn.component';
import { ListIsbnComponent } from './views/list-isbn/list-isbn.component';
import { MatProgressBarModule, MatSelectModule } from '@angular/material';
import { IsLoadingModule } from '@service-work/is-loading';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { WaitingBoxComponent } from './views/waiting-box/waiting-box.component';
import { JoinUsComponent } from './views/home/join-us/join-us.component';
import { HelpComponent } from './views/home/help/help.component';
import { EditLicenceComponent } from './views/licence/edit-licence/edit-licence.component';
import { ProgressLayerComponent } from './views/waiting-box/progress-layer/progress-layer.component';

/**
 * Module principal
 */
@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(), ReactiveFormsModule,
    ModalModule.forRoot(), AlertModule.forRoot(), HttpClientModule,
    ChartsModule, FormsModule, HttpClientModule, RouterModule, DataTablesModule,
    MatSliderModule, MatInputModule, MatAutocompleteModule, MatFormFieldModule, MatMenuModule, IsLoadingModule, MatProgressBarModule,
    MatProgressSpinnerModule, MatSelectModule
  ],
  declarations: [
    AppComponent, P404Component, P500Component,
    HeaderComponent, FooterComponent,
    LoginComponent, RegisterComponent, AlertDialogComponent, HomeComponent, NumericDirective, DashboardComponent, DepolComponent,
    DossierComponent, LicenceComponent, ClaimComponent, ListDepotComponent, DmdIsbnComponent, NewIsbnComponent, ListIsbnComponent,
    WaitingBoxComponent, JoinUsComponent, HelpComponent, EditLicenceComponent, ProgressLayerComponent
  ],
  providers: [GenericDaoService, GenericManagerService, LoginDaoService, LoginManagerService, AuthGuard,
    {provide: LocationStrategy, useClass: PathLocationStrategy}],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
