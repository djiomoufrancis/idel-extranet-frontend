import { Component, OnInit, ElementRef, HostListener} from '@angular/core';
import { LoginManagerService } from '../../../core/services/business/login-manager.service';
import * as cfg from '../../../../assets/config.json';

/**
 * Composant Entete de la page d'accueil
 */
@Component({
  selector: 'app-header2',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private el: ElementRef, private login: LoginManagerService) { }

  ngOnInit() {
  }

  /**
   * Affiche du menu deroulant a l'approche de la souris
   * @param id
   */
  @HostListener('mouseenter')
  onMouseEnter(id: any) {
    if (id === null || id === undefined) { return; }
    const strTitreMenuID = document.getElementById(id).id;
    if (strTitreMenuID === undefined) { return; }
    const strSous_TitreMenuID = 'sous_' + strTitreMenuID;
    document.getElementById(strSous_TitreMenuID).setAttribute('style', 'zIndex: 100');
    document.getElementById(strSous_TitreMenuID).setAttribute('style', 'display: block');
  }

  /**
   * Masquage du menu deroulant a la sortie de la souris
   * @param id
   */
  @HostListener('mouseout') onmouseout(id: any) {
    if (id === null || id === undefined) { return; }
    const strTitreMenuID = document.getElementById(id).id;
    if (strTitreMenuID === undefined) { return; }
    const strSous_TitreMenuID = 'sous_' + strTitreMenuID;
    document.getElementById(strSous_TitreMenuID).style.setProperty('display', 'none');
  }

  /**
   * Deconnexion a l'application
   */
  logout() {
    this.login.logout();
  }

  get logoutUrl(): string {
    return cfg.logoutUrl;
  }

}
