import { Component, OnInit } from '@angular/core';

/**
 * Composant Pied de la page d'accueil
 */
@Component({
  selector: 'app-footer2',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
