import { Component, OnInit } from '@angular/core';
import { isLoggedIn, getUid } from '../../../core/shared/utils/helper';

/**
 * Composant Contenu de la page d'accueil
 */
@Component({
  selector: 'app-dashboard2',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  isConnected() {
    return isLoggedIn();
  }
}
