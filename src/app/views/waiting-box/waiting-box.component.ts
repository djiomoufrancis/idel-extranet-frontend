import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { WaitingBoxService } from './waiting-box.service';

@Component({
  selector: 'app-waiting-box',
  templateUrl: './waiting-box.component.html',
  styleUrls: ['./waiting-box.component.scss']
})
export class WaitingBoxComponent implements OnInit {

  private _visible = false;

    constructor(private _waitingBoxService: WaitingBoxService) {
    }

    ngOnInit() {
        this._waitingBoxService.showProgressLayer.subscribe((service: any) => {
            this._visible = true;
        });

        this._waitingBoxService.hideProgressLayer.subscribe((service: any) => {
            this._visible = false;
        });
    }

    get visible(): boolean {
        return this._visible;
    }


}
