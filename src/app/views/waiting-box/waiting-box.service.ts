import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WaitingBoxService {

  private _showProgressLayerEvent = new EventEmitter<WaitingBoxService>();
  private _hideProgressLayerEvent = new EventEmitter<WaitingBoxService>();
  private _activeCount = 0;
  private _timeoutHandle: any = null;

  constructor() {
  }

  public get showProgressLayer(): EventEmitter<WaitingBoxService> {
      return this._showProgressLayerEvent;
  }

  public get hideProgressLayer(): EventEmitter<WaitingBoxService> {
      return this._hideProgressLayerEvent;
  }

  show(delay: number = 1000) {
      this._activeCount ++;
      if (this._activeCount === 1) {

          if (this._timeoutHandle) {
              clearTimeout(this._timeoutHandle);
          }

          this._timeoutHandle = setTimeout(() => {
              this._showProgressLayerEvent.emit(this);
              this._timeoutHandle = null;
          }, delay);
      }
  }

  hide() {
      if (this._activeCount > 0) {
          this._activeCount --;

          if (this._activeCount === 0) {
              if (this._timeoutHandle) {
                  clearTimeout(this._timeoutHandle);
                  this._timeoutHandle = null;
              } else {
                  this._hideProgressLayerEvent.emit(this);
              }
          }
      }
  }


}
