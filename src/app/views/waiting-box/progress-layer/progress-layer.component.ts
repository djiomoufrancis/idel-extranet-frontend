import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { ProgressLayerDisplayMode } from './progress-activity-display-mode';

@Component({
  selector: 'app-banq-progress-layer',
  templateUrl: './progress-layer.component.html',
  styleUrls: ['./progress-layer.component.scss']
})
export class ProgressLayerComponent implements OnInit {
  private _visibilityChangedEvent = new EventEmitter<ProgressLayerComponent>();

  private _displayMode: ProgressLayerDisplayMode =
    ProgressLayerDisplayMode.region;
  private _diameter = 100;
  private _visibilityCount = 0;
  private _visible = false;
  private _color = 'primary';

  constructor() {}

  ngOnInit() {}

  get onVisibilityChanged(): EventEmitter<ProgressLayerComponent> {
    return this._visibilityChangedEvent;
  }

  show() {
    this._visibilityCount++;
    if (this._visibilityCount === 1) {
      this._visible = true;
      this._visibilityChangedEvent.emit(this);
    }
  }

  hide() {
    this._visibilityCount--;
    if (this._visibilityCount === 0) {
      this._visible = false;
      this._visibilityChangedEvent.emit(this);
    }
  }

  get fullscreen(): boolean {
    return this._displayMode === ProgressLayerDisplayMode.fullscreen;
  }

  get displayMode(): ProgressLayerDisplayMode {
    return this._displayMode;
  }

  get visible(): boolean {
    return this._visible;
  }

  @Input() set displayMode(value: ProgressLayerDisplayMode) {
    this._displayMode = value;
  }

  get color(): string {
    return this._color;
  }

  @Input() set color(clr: string) {
    this._color = clr;
  }

  get diameter(): number {
    return this._diameter;
  }

  @Input() set diameter(value: number) {
    this._diameter = value;
  }
}
