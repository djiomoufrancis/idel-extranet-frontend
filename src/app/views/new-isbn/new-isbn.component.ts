import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-isbn',
  templateUrl: './new-isbn.component.html',
  styleUrls: ['./new-isbn.component.scss']
})
export class NewIsbnComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }


  back() {
    this.router.navigate(['/home/dashboard']);
  }

}
