import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginManagerService } from '../../core/services/business/login-manager.service';
import { Alert } from '../../core/models/alert';
import { LoginRequest } from '../../core/models/login-request';
import { threatException } from '../../core/shared/utils/helper';

/**
 * Composant d'authentification d'un utilisateur a l'application
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  disp: boolean;
  myAlert: Alert;
  request: LoginRequest;
  constructor(private router: Router, private loginService: LoginManagerService) {}

  /**
   * initialisation du composant
   */
  ngOnInit(): void {
    this.request = new LoginRequest('', '');
    this.disp = false;
  }

  /**
   * Effectue l'authentification de l'utilisateur sur la base des parametres saisis
   */
  login() {
    if (this.request.username === 'admin' && this.request.password === 'admin') {
      localStorage.setItem('token', '21232f297a57a5a743894a0e4a801fc3');
      this.router.navigate(['/home/dashboard']);
    } else {
      this.myAlert = threatException('Invalid Username or Password');
    }
    /*
    try {
      this.loginService.login( this.request );
      this.router.navigate(['/dashboard']);
    } catch (error) {
      this.myAlert = threatException(error);
    }
    */
  }

  /**
   * Deconnexion
   */
  logout() {
    this.loginService.logout();
  }

  /**
   * Navigation vers la page d'inscription
   */
  register() {
    this.router.navigate(['/register']);
  }
  /**
   * Afficher/Masquer le panneur de description de comment s'inscrire
   */
  onCmtAbonneClick() {
    this.disp = !this.disp;
  }
}
