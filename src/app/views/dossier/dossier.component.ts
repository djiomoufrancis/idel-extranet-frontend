import { Component, OnInit } from '@angular/core';
import { Editor } from '../../core/data/editor';
import { Alert } from '../../core/models/alert';
import { State } from '../../core/models/state';
import { City } from '../../core/models/city';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { STATES } from '../../core/shared/mock/states';
import { AlertType } from '../../core/enums/alert-type.enum';
import { Respondent } from '../../core/data/respondent';
import { LoginManagerService } from '../../core/services/business/login-manager.service';
import { EditorDaoService } from '../../core/services/dao/editor-dao.service';
import { WaitingBoxService } from '../waiting-box/waiting-box.service';
import { ApiResponse } from '../../core/models/api-response';
import { debounceTime, tap, startWith, map, isEmpty } from 'rxjs/operators';
import { getUid } from '../../core/shared/utils/helper';
import { CITIES } from '../../core/shared/mock/cities';

/**
 * Composant de gestion du dossier d'un editeur
 */
@Component({
  selector: 'app-dossier',
  templateUrl: './dossier.component.html',
  styleUrls: ['./dossier.component.scss']
})
export class DossierComponent implements OnInit {

  editor: Editor;
  myAlert: Alert;
  states: Array<State> = [];
  cities: Observable<City[]>;
  registerForm: FormGroup;
  errs: string[];
  updatingIsOngoing = false;
  searchCitiesCtrl = new FormControl();
  phone: { prefx: string, sufx: string, racine: string };
  telecop: { prefx: string, sufx: string, racine: string };

  constructor(private router: Router, private fb: FormBuilder,
    private login: LoginManagerService,
    private _waitingBox: WaitingBoxService,
    private editorDAO: EditorDaoService,
    private editorService: EditorDaoService) {
    this.createForm();
  }

  /**
   * Initialisation du formulaire
   */
  ngOnInit() {

    // Gets the connected Editor
    this.editor = this.login.USER_EDITOR;

    // Load all the states
    this.states = STATES;

    // Init Alert frame
    this.myAlert = new Alert('');

    // Initialize the list of validation errors
    this.errs = [];

    // Check if there is an update request pending
    this._waitingBox.show();
    this.editorDAO.isUpdateRequestPending(getUid()).subscribe(
      (response) => {
        const resp: ApiResponse = response;
        this.updatingIsOngoing = resp != null && resp.success ? (resp.data as boolean) : false;
      },
      (error) => {
        console.log(error);
      },
      () => {
        this._waitingBox.hide();
      }
    );

    // Load Cities
    this.cities = this.searchCitiesCtrl.valueChanges.pipe(debounceTime(500),
      tap(() => { }), startWith(''), map(value => this._filter(value)));

    // Load respondant details
    for (const r of this.editor.repondants) { this.add(r); }

    // Build Phone model
    if (this.editor.telephone != null && this.editor.telephone.length > 9) {
      this.phone = {
        prefx: this.editor.telephone.substr(0, 3),
        sufx: this.editor.telephone.substr(3, 3),
        racine: this.editor.telephone.substr(6, 4)
      };
    }

    // Build Telecopieur model
    if (this.editor.telecopieur != null && this.editor.telecopieur.length > 9) {
      this.telecop = {
        prefx: this.editor.telecopieur.substr(0, 3),
        sufx: this.editor.telecopieur.substr(3, 3),
        racine: this.editor.telecopieur.substr(6, 4)
      };
    }
  }
  /**
   * Filter the autocomplete list of towns
   * @param value the textfield value
   */
  private _filter(value: string): City[] {
    const filterValue = value.toLowerCase();
    return CITIES.filter(option => option.city.toLowerCase().includes(filterValue));
  }

  /**
   * Initialisation du formulaire dynamique de gestion des repondants
   */
  createForm() {
    this.registerForm = this.fb.group({
      title: [null, Validators.required],
      firstname: [null, [Validators.required, Validators.minLength(2)]],
      lastname: [null, [Validators.required, Validators.minLength(2)]],
      email: [null, [Validators.required, Validators.minLength(2)]],
      phone: [null, [Validators.required, Validators.pattern('^\d{10}$')]],
      phoneArea: [null, [Validators.required, Validators.pattern('^\d{3}$')]],
      phone3: [null, [Validators.required, Validators.pattern('^\d{3}$')]],
      phone4: [null, [Validators.required, Validators.pattern('^\d{4}$')]],
      poste: [null, []],
      principal: [false, []],
      persons: this.fb.array([])
    });
    this.phone = { prefx: '', sufx: '', racine: '' };
    this.telecop = { prefx: '', sufx: '', racine: '' };
  }

  /**
   * Retourne la liste des controles de saisie des repondants
   */
  get persons() {
    return this.registerForm.get('persons') as FormArray;
  }

  /**
    * Suppression du repondant
    * @param id Numero de ligne
    */
  remove(id: any) {
    this.persons.removeAt(id);
  }

  /**
   * Ajout d'un nouveau Repondant dans la liste
   */
  add(r?: Respondent) {
    if (r == null || r === undefined) { r = new Respondent(); }
    this.persons.push(this.fb.group({
      title: [r.titre, Validators.required],
      firstname: [r.nom, [Validators.required, Validators.minLength(2)]],
      lastname: [r.prenom, [Validators.required, Validators.minLength(2)]],
      email: [r.courriel, [Validators.required, Validators.minLength(2)]],
      phone: [r.telephone, [Validators.required]],
      phoneArea: [r.telephone != null && r.telephone.length > 9 ? r.telephone.substr(0, 3) : '', [Validators.required, Validators.pattern('^\d{3}$')]],
      phone3: [r.telephone != null && r.telephone.length > 9 ? r.telephone.substr(3, 3) : '', [Validators.required, Validators.pattern('^\d{3}$')]],
      phone4: [r.telephone != null && r.telephone.length > 9 ? r.telephone.substr(6, 4) : '', [Validators.required, Validators.pattern('^\d{4}$')]],
      poste: [r.poste, []],
      principal: [r.principal, []]
    }));
  }

  /**
   * Lecture des infos saisies sur le formulaire
   */
  readPersons() {
    this.errs = [];
    this.editor.repondants = new Array<Respondent>();
    this.editor.telephone = this.phone.prefx + this.phone.sufx + this.phone.racine;
    this.editor.telecopieur = this.telecop.prefx + this.telecop.sufx + this.telecop.racine;
    for (const p of this.persons.controls) {
      const r = new Respondent();
      r.titre = p.get('title').value;
      r.nom = p.get('firstname').value;
      r.prenom = p.get('lastname').value;
      r.telephone = p.get('phoneArea').value + p.get('phone3').value + p.get('phone4').value;
      r.courriel = p.get('email').value;
      r.poste = p.get('poste').value;
      if (r.titre == null || r.titre === '') { this.errs.push('Veuillez cocher l\'option Mme ou M. pour chacune des personnes ajoutées dans le formulaire.'); }
      if (r.nom == null || r.nom.trim() === '') { this.errs.push('Veuillez entrer une donnée dans le champ Nom.'); }
      if (r.prenom == null || r.prenom.trim() === '') { this.errs.push('Veuillez entrer une donnée dans le champ Prénom.'); }
      if (r.courriel == null || r.courriel.trim() === '') { this.errs.push('Vous devez spécifier l\'adresse courriel du répondant principal.'); }
      this.editor.repondants.push(r);
    }

    if (this.editor.nom == null || this.editor.nom.trim() === '') { this.errs.push('Veuillez entrer une donnée dans le champ Éditeur.'); }
    if (this.editor.adresse1 == null || this.editor.adresse1.trim() === '') { this.errs.push('Veuillez entrer une donnée dans le champ Adresse postale.'); }
    if (this.editor.ville == null || this.editor.ville.trim() === '') { this.errs.push('Veuillez entrer une donnée dans le champ Ville.'); }
    if (this.editor.codePostal.length < 6 || this.editor.codePostal.length > 7) { this.errs.push('Le code postal entré est invalide.'); }
    if (this.editor.telephone.length !== 10 || this.editor.telephone.trim() === '') { this.errs.push('Le numéro de téléphone de l\'éditeur est invalide : il doit comporter 10 chiffres dont 3 pour le code régional.'); }

  }

  /**
   * Validation du formulaire d'edition des coordonnees de l'editeur
   */
  submit() {
    this.readPersons();
    if (this.errs.length > 0) { return; }
    this._waitingBox.show();
    this.editorService.updateEditor(this.editor).subscribe(
      (response) => {
        const resp: ApiResponse = response;
        if (resp.success) {
          this.editor = new Editor().fillFromJSON(resp.data.toString());
          this.myAlert.displayAlert(AlertType.success, 'Votre demande de modification a été enregistrée avec succes!');
        } else {
          this.myAlert.displayAlert(AlertType.error, 'Une erreur s\'est produite. Echec de la mise a jour: ' + resp.message);
        }
      },
      (error) => {
        this.myAlert.displayAlert(AlertType.error, 'Une erreur s\'est produite. Echec de la mise a jour: ' + error.message);
      },
      () => {
        this._waitingBox.hide();
      }
    );
  }

  /**
   * Retour a la page d'accueil
   */
  back() {
    this.router.navigate(['/home/dashboard']);
  }

}
