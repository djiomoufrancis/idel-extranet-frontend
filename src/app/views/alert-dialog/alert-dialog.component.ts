import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Alert } from '../../core/models/alert';
import { AlertType } from '../../core/enums/alert-type.enum';
import { ModalDirective } from 'ngx-bootstrap/modal';

/**
 * Composant de gestion des notifications utilisateurs
 */
@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.scss']
})
export class AlertDialogComponent implements OnInit {

  @ViewChild('dlgAlert', { static: false }) public dlgAlert: ModalDirective;
  @Input() public myAlert: Alert;

  constructor() { }

  ngOnInit() {
    if (this.myAlert === null || this.myAlert === undefined) {
      this.myAlert = new Alert('Everything is fine');
    }
  }

  getStyleClassSuffix() {
    switch (this.myAlert.type) {
      case AlertType.info: return 'info';
      case AlertType.error: return 'danger';
      case AlertType.warning: return 'warning';
      case AlertType.confirm: return 'primary';
      case AlertType.success: return 'success';
    }
  }

}
