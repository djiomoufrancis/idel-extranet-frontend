import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-isbn',
  templateUrl: './list-isbn.component.html',
  styleUrls: ['./list-isbn.component.scss']
})
export class ListIsbnComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }


  back() {
    this.router.navigate(['/home/dashboard']);
  }

}
