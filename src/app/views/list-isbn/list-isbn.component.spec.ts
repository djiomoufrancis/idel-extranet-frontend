/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ListIsbnComponent } from './list-isbn.component';

describe('ListIsbnComponent', () => {
  let component: ListIsbnComponent;
  let fixture: ComponentFixture<ListIsbnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListIsbnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListIsbnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
