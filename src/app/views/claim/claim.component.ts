import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Composant de consultation des reclamations des editeurs
 */
@Component({
  selector: 'app-claim',
  templateUrl: './claim.component.html',
  styleUrls: ['./claim.component.scss']
})
export class ClaimComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

  }

  back() {
    this.router.navigate(['/home/dashboard']);
  }

}
