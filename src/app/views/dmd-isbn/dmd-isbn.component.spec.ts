/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DmdIsbnComponent } from './dmd-isbn.component';
import { Router } from '@angular/router';

describe('DmdIsbnComponent', () => {
  let component: DmdIsbnComponent;
  let fixture: ComponentFixture<DmdIsbnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ Router ],
      declarations: [ DmdIsbnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmdIsbnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
