import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dmd-isbn',
  templateUrl: './dmd-isbn.component.html',
  styleUrls: ['./dmd-isbn.component.scss']
})
export class DmdIsbnComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  back() {
    this.router.navigate(['/home/dashboard']);
  }

}
