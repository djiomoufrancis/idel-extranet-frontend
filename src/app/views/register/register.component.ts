import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Editor } from '../../core/data/editor';
import { Respondent } from '../../core/data/respondent';
import { Country } from '../../core/models/country';
import { State } from '../../core/models/state';
import { City } from '../../core/models/city';
import { GenericManagerService } from '../../core/services/business/generic-manager.service';
import { LoginManagerService } from '../../core/services/business/login-manager.service';
import { Alert } from '../../core/models/alert';
import { AlertType } from '../../core/enums/alert-type.enum';
import {
  FormGroup,
  FormBuilder,
  FormArray,
  Validators,
  FormControl
} from '@angular/forms';
import { LoginDaoService } from '../../core/services/dao/login-dao.service';
import { CITIES } from '../../core/shared/mock/cities';
import { debounceTime, tap, startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { STATES } from '../../core/shared/mock/states';
import { ApiResponse } from '../../core/models/api-response';
import { WaitingBoxService } from '../waiting-box/waiting-box.service';
// import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

/**
 * Composant Fiche d'inscription
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  isLoading: boolean;
  editor = new Editor();
  saved: Editor;
  myAlert: Alert;
  personList: Array<Respondent> = [];
  awaitingPersonList: Array<Respondent> = [];
  editField: string;
  countries: Array<Country> = [];
  states: Array<State> = [];
  cities: Observable<City[]>;
  registerForm: FormGroup;
  errs: string[];
  phone: { prefx: ''; sufx: ''; racine: '' };
  telecop: { prefx: ''; sufx: ''; racine: '' };

  searchCitiesCtrl = new FormControl();
  errorMsg: string[] = [];

  constructor(
    private router: Router,
    private generic: GenericManagerService,
    private loginService: LoginManagerService,
    private fb: FormBuilder,
    private loginDAO: LoginDaoService,
    private _loading: WaitingBoxService
  ) {
    this.createForm();
    this.errs = [];
  }

  ngOnInit() {
    try {
      this.isLoading = false; // this.isLoadingService.isLoading$().;
      this.countries = this.generic.loadCountries();
      this.states = STATES; // this.generic.loadStates();
      this.reset();
      this.add();
      this.myAlert = new Alert('Everything is fine!');
    } catch (error) {
      console.log('Une erreur sest produite: ', error);
      this.myAlert.displayAlert(AlertType.error, error);
    }

    this.cities = this.searchCitiesCtrl.valueChanges.pipe(
      debounceTime(500),
      tap(() => {}),
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): City[] {
    const filterValue = value.toLowerCase();
    return CITIES.filter(option =>
      option.city.toLowerCase().includes(filterValue)
    );
  }
  displayFn(city?: City): string | undefined {
    return city ? city.city : undefined;
  }

  /**
   * Initialisation du formulaire dynamique des gestion des repondants
   */
  createForm() {
    this.registerForm = this.fb.group({
      title: [null, Validators.required],
      firstname: [null, [Validators.required, Validators.minLength(2)]],
      lastname: [null, [Validators.required, Validators.minLength(2)]],
      email: [null, [Validators.required, Validators.minLength(2)]],
      phone: [null, [Validators.required, Validators.pattern('^d{10}$')]],
      phoneArea: [null, [Validators.required, Validators.pattern('^d{3}$')]],
      phone3: [null, [Validators.required, Validators.pattern('^d{3}$')]],
      phone4: [null, [Validators.required, Validators.pattern('^d{4}$')]],
      poste: [null, []],
      persons: this.fb.array([])
    });
  }

  /**
   * Retourne la liste des champs de saisie decrivant les repondants
   */
  get persons() {
    return this.registerForm.get('persons') as FormArray;
  }

  /**
   * Suppresion d'un repondant de la liste
   * @param id identifiant de la ligne
   */
  remove(id: any) {
    this.persons.removeAt(id);
  }

  /**
   * Ajout d'un nouveau repondant dans la liste
   */
  add() {
    const r = new Respondent();
    this.persons.push(
      this.fb.group({
        title: [r.titre, Validators.required],
        firstname: [r.nom, [Validators.required, Validators.minLength(2)]],
        lastname: [r.prenom, [Validators.required, Validators.minLength(2)]],
        email: [r.courriel, [Validators.required, Validators.minLength(2)]],
        phone: [r.telephone, [Validators.required]],
        phoneArea: [
          r.telephone,
          [Validators.required, Validators.pattern('^d{3}$')]
        ],
        phone3: [
          r.telephone,
          [Validators.required, Validators.pattern('^d{3}$')]
        ],
        phone4: [
          r.telephone,
          [Validators.required, Validators.pattern('^d{4}$')]
        ],
        poste: [r.poste, []]
      })
    );
  }

  /**
   * Lecture des donnees resseignees sur le formulaire
   */
  readPersons() {
    this.errs = [];
    this.editor.repondants = new Array<Respondent>();
    this.editor.telephone =
      this.phone.prefx + this.phone.sufx + this.phone.racine;
    this.editor.telecopieur =
      this.telecop.prefx + this.telecop.sufx + this.telecop.racine;
    for (const p of this.persons.controls) {
      const r = new Respondent();
      r.titre = p.get('title').value;
      r.nom = p.get('firstname').value;
      r.prenom = p.get('lastname').value;
      r.telephone =
        p.get('phoneArea').value +
        p.get('phone3').value +
        p.get('phone4').value;
      r.courriel = p.get('email').value;
      r.poste = p.get('poste').value;
      if (r.titre == null || r.titre === '') {
        this.errs.push(
          'Veuillez cocher l\'option Mme ou M. pour chacune des personnes ajoutées dans le formulaire.'
        );
      }
      if (r.nom == null || r.nom.trim() === '') {
        this.errs.push('Veuillez entrer une donnée dans le champ Nom.');
      }
      if (r.prenom == null || r.prenom.trim() === '') {
        this.errs.push('Veuillez entrer une donnée dans le champ Prénom.');
      }
      if (r.courriel == null || r.courriel.trim() === '') {
        this.errs.push(
          'Vous devez spécifier l\'adresse courriel du répondant principal.'
        );
      }
      this.editor.repondants.push(r);
    }

    if (this.editor.nom == null || this.editor.nom.trim() === '') {
      this.errs.push('Veuillez entrer une donnée dans le champ Éditeur.');
    }
    if (this.editor.adresse1 == null || this.editor.adresse1.trim() === '') {
      this.errs.push(
        'Veuillez entrer une donnée dans le champ Adresse postale.'
      );
    }
    if (this.editor.ville == null || this.editor.ville.trim() === '') {
      this.errs.push('Veuillez entrer une donnée dans le champ Ville.');
    }
    if (
      this.editor.codePostal.length < 6 ||
      this.editor.codePostal.length > 7
    ) {
      this.errs.push('Le code postal entré est invalide.');
    }
    if (
      this.editor.telephone.length !== 10 ||
      this.editor.telephone.trim() === ''
    ) {
      this.errs.push(
        'Le numéro de téléphone de l\'éditeur est invalide : il doit comporter 10 chiffres dont 3 pour le code régional.'
      );
    }
  }

  /**
   * Soumission du formulaire d'inscription a l'enregistrement
   */
  submit() {
    try {
      this.readPersons();
      if (this.errs.length > 0) {
        return;
      }
      this.showProgressBar();
      this.loginDAO.callRegisterAPI(this.editor).subscribe(
        response => {
          const resp: ApiResponse = response;
          if (resp.success) {
            this.saved = resp.data;
            this.myAlert.displayAlert(
              AlertType.success,
              'Inscription terminee avec succes! Veuillez consulter votre courriel pour recevoir vos parametres de connexion. Votre identifiant est: ' +
                this.saved.idEditeur
            );
            this.reset();
          } else {
            this.saved = null;
            this.myAlert.displayAlert(
              AlertType.error,
              'Une erreur s\'est produite: ' + resp.message
            );
          }
        },
        error => {
          this.myAlert.displayAlert(
            AlertType.error,
            'Une erreur s\'est produite. Echec de la mise a jour: ' +
              error.message
          );
        },
        () => {
          this.hideProgressBar();
        }
      );
    } catch (error) {
      this.myAlert.displayAlert(AlertType.error, error);
      console.log('Une erreur sest produite: ', error);
    }
  }

  /**
   * Reinitialisation du formulaire d'inscription
   */
  reset() {
    this.editor = new Editor();
    this.registerForm.reset();
    this.phone = { prefx: '', sufx: '', racine: '' };
    this.telecop = { prefx: '', sufx: '', racine: '' };
  }

  /**
   * Retour a la page de connexion
   */
  back() {
    this.router.navigate(['/login']);
  }

  showProgressBar() {
    this._loading.show();
  }

  hideProgressBar() {
    this._loading.hide();
  }

  /* openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  } */
}
