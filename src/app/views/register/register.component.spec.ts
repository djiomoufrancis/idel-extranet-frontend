import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Editor } from '../../core/data/editor';
import { Respondent } from '../../core/data/respondent';
import { Country } from '../../core/models/country';
import { State } from '../../core/models/state';
import { City } from '../../core/models/city';
import { GenericManagerService } from '../../core/services/business/generic-manager.service';
import { LoginManagerService } from '../../core/services/business/login-manager.service';
import { Alert } from '../../core/models/alert';
import { AlertType } from '../../core/enums/alert-type.enum';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { LoginDaoService } from '../../core/services/dao/login-dao.service';
import { CITIES } from '../../core/shared/mock/cities';
import { debounceTime, tap, startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { STATES } from '../../core/shared/mock/states';
import { RegisterComponent } from './register.component';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

describe('Register Component', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule, HttpClientModule, RouterModule
      ],
      declarations: [
        RegisterComponent, AlertDialogComponent
      ],
      providers: [LoginManagerService, GenericManagerService, LoginDaoService]
    }).compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(RegisterComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('Register Component Created', () => {
      expect(component).toBeTruthy();
    });

    it('Register Component initialisation', () => {
      component.ngOnInit();
      expect(component.countries).toBeDefined();
      expect(component.states).toBeDefined();
      expect(component.cities).toBeDefined();
      expect(component.myAlert).toBeDefined();
      expect(component.registerForm).toBeDefined();
    });

    it('Adding new Respondant on Register Form', () => {
      component.add();
      console.log('After adding, Number of respondants is: ', component.persons.length);
    });

    it('Removing Respondant on Register Form', () => {
      component.remove(1);
      console.log('After removing, Number of respondants is: ', component.persons.length);
    });

    it('Try to submit Register Form', () => {
      component.submit();
      expect(component.errs).toBeDefined();
      console.log('Display list of errors after trying to submit form: ', component.errs);
    });


});
