import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Composant de consultation des licences de la BANQ
 */
@Component({
  selector: 'app-licence',
  templateUrl: './licence.component.html',
  styleUrls: ['./licence.component.scss']
})
export class LicenceComponent implements OnInit {

  licences: string[];

  constructor(private router: Router) {
    this.licences = [];
  }

  ngOnInit() {
    this.licences.push('P-1843-20140204.txt');
    this.licences.push('P-1879-20140317.txt');
    this.licences.push('LPW-1880-20140317.txt');
    this.licences.push('LPW-1881-20140317.txt');
    this.licences.push('LPW-1882-20140317.txt');
    this.licences.push('LPW-1883-20140317.txt');
    this.licences.push('LPW-2108-20140925.txt');
    this.licences.push('LPW-1735-20131018.txt');
    this.licences.push('LPW-1841-20140204.txt');
    this.licences.push('LPW-1894-20140328.txt');
    this.licences.push('LPW-2107-20140925.txt');
    this.licences.push('LPW-2362-20150421.txt');
    this.licences.push('LPW-2490-20150715.txt');
    this.licences.push('LPW-2366-20150421.txt');
    this.licences.push('LPW-2367-20150421.txt');
    this.licences.push('LPW-2368-20150421.txt');
    this.licences.push('LPW-2369-20150421.txt');
    this.licences.push('LPW-2372-20150421.txt');
    this.licences.push('LPW-3110-20160209.txt');
    this.licences.push('LPW-3111-20160209.txt');
    this.licences.push('LPW-3129-20160215.txt');
    this.licences.push('LPW-3346-20160816.txt');
    this.licences.push('LPW-3358-20160825.txt');
  }

  back() {
    this.router.navigate(['/home/dashboard']);
  }

}
