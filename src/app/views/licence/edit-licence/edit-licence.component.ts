import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-licence',
  templateUrl: './edit-licence.component.html',
  styleUrls: ['./edit-licence.component.scss']
})
export class EditLicenceComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  submit() {

  }

  back() {
    this.router.navigate(['/home/dashboard']);
  }

}
