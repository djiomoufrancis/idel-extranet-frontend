import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-depot',
  templateUrl: './list-depot.component.html',
  styleUrls: ['./list-depot.component.scss']
})
export class ListDepotComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

  }

  back() {
    this.router.navigate(['/home/dashboard']);
  }

}
