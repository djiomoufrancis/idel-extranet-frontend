import { Component, OnInit } from '@angular/core';
import { Alert } from '../../core/models/alert';
import { Router } from '@angular/router';

/**
 * Composant de gestion du Depot Legal
 */
@Component({
  selector: 'app-depol',
  templateUrl: './depol.component.html',
  styleUrls: ['./depol.component.scss']
})
export class DepolComponent implements OnInit {

  errs: string[];
  myAlert = new Alert('');

  constructor(private router: Router) { }

  ngOnInit() {
    this.errs = [];
    this.myAlert = new Alert('');
  }

  back() {
    this.router.navigate(['/home/dashboard']);
  }

}
