
## Table of Contents

* [Description](#Description)
* [Dependances](#Dependances)
* [Prerequisites](#Prerequisites)
* [Installation](#installation)
* [Usage](#usage)
* [What's included](#whats-included)
* [Comments](#Comments)
* [Documentation](#documentation)
* [Contributing](#contributing)
* [Versioning](#versioning)
* [Creators](#creators)
* [License](#license)
* [Support Development](#support-development)


## Description
Le module de l'extranet des éditeurs est la solution principale pour les éditeurs faisant partie du système IDEL. Il s'agit d'une application web, disponible via un extranet, permettant à un éditeur de consulter et de modifier ses informations personelles, de consulter la liste des dépôts qui a effectué, de créer un nouveau dépôt, que ce soit pour des exemplaires physiques ou des versions numériques, de consulter les réclamations et de faire des demandes ISBN.
Le module inclut le formulaire d'inscription à l'extranet, permettant à un éditeur d'obtenir un compte pour y accéder.


## Dependances

* 💪  [ngx-bootstrap](https://www.npmjs.com/package/ngx-bootstrap)
* 💪  [Angular Material](https://material.angular.io/)
* 💪  [ngx-translate](http://www.ngx-translate.com/)
* 💪  [rxjs](https://github.com/ReactiveX/rxjs)
* 💪  [compodoc](https://compodoc.app/)
* 💪  [coreui](https://coreui.io/pro/angular)
* 💪  [HammerJs](https://hammerjs.github.io/)
* 💪  [JQuery](https://jquery.com/)


#### Prerequisites
Before you begin, make sure your development environment includes `Node.js®` , an `npm` and / or an `yarn` package manager.

###### Node.js
Angular requires `Node.js` version 8.x or 10.x

- To check your version, run `node -v` in a terminal/console window.
- To get `Node.js`, go to [nodejs.org](https://nodejs.org/).

###### Angular CLI
Install the Angular CLI globally using a terminal/console window.
```bash
npm install -g @angular/cli
```

##### Update to Angular 8
Angular 8 requires `Node.js` version 12.x   
Update guide - see: [https://update.angular.io](https://update.angular.io)

## Installation

### Clone repo

``` bash
# clone the repo
$ git clone https://you@bitbucket.org/djiomoufrancis/idel-extranet-frontend.git

# go into app's directory
$ cd idel-extranet-frontend

# install app's dependencies
$ yarn
```

## Usage

``` bash
# serve with hot reload at localhost:4200.
$ yarn start

# build for production with minification
$ yarn build

# Generate dist for Prod
$ yarn build --base-href /idel-v2/extranet/

# Generate documentation
$ yarn docs

# Runs linting tools to validate your code before pushing your branch
$ yarn lint:fix
$ yarn lint

```


## What's included

Within the download you'll find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You'll see something like this:

```
idel-extranet-frontend/
├── docs/
├── e2e/
├── src/
│   ├── app/
│   │   ├── core/
│   │   │   ├── data/
│   │   │   │   ├── editor.ts
│   │   │   │   ├── lang.ts
│   │   │   │   ├── respondent.ts
│   │   │   │   └── user.ts
│   │   │   ├── enums/
│   │   │   │   ├── alert-type.enum.ts
│   │   │   │   └── lang.enum.ts
│   │   │   ├── models/
│   │   │   │   ├── alert.ts
│   │   │   │   ├── city.ts
│   │   │   │   ├── country.ts
│   │   │   │   ├── login-request.ts
│   │   │   │   ├── login-response.ts
│   │   │   │   ├── resetPwd-request.ts
│   │   │   │   ├── signup-request.ts
│   │   │   │   └── state.ts
│   │   │   ├── services/
│   │   │   │   ├── business/
│   │   │   │   │   ├── generic-manager.service.ts
│   │   │   │   │   └── login-manager.service.ts
│   │   │   │   ├── dao/
│   │   │   │   │   ├── generic-dao.service.ts
│   │   │   │   │   └── login-dao.service.ts
│   │   │   │   └── interfaces/
│   │   │   │       ├── generic-interface.ts
│   │   │   │       └── login-interface.ts
│   │   │   └── shared/
│   │   │   │   ├── directives/
│   │   │   │   │   └── numeric.directive.ts
│   │   │   │   ├── mock/
│   │   │   │   ├── pipes/
│   │   │   │   └── utils/
│   │   │   │       ├── auth.guard.ts
│   │   │   │       ├── helper.ts
│   │   │   │       └── serializable.ts
│   │   ├── modules/
│   │   │   ├── login/
│   │   │   └── language-translation.module.ts
│   │   ├── views/
│   │   │   ├── alert-dialog/
│   │   │   │   ├── alert-dialog.component.css
│   │   │   │   ├── alert-dialog.component.html
│   │   │   │   └── alert-dialog.component.ts
│   │   │   ├── claim/
│   │   │   │   ├── claim.component.css
│   │   │   │   ├── claim.component.html
│   │   │   │   └── claim.component.ts
│   │   │   ├── depol/
│   │   │   │   ├── depol.component.css
│   │   │   │   ├── depol.component.html
│   │   │   │   └── depol.component.ts
│   │   │   ├── dossier/
│   │   │   │   ├── dossier.component.css
│   │   │   │   ├── dossier.component.html
│   │   │   │   └── dossier.component.ts
│   │   │   ├── error/
│   │   │   │   ├── 404.component.ts
│   │   │   │   ├── 404.component.html
│   │   │   │   ├── 500.component.html
│   │   │   │   └── 500.component.ts
│   │   │   ├── home/
│   │   │   │   ├── header/
│   │   │   │   │   ├── header.component.css
│   │   │   │   │   ├── header.component.html
│   │   │   │   │   └── header.component.ts
│   │   │   │   ├── dashboard/
│   │   │   │   │   ├── dashboard.component.css
│   │   │   │   │   ├── dashboard.component.html
│   │   │   │   │   └── dashboard.component.ts
│   │   │   │   ├── footer/
│   │   │   │   │   ├── footer.component.css
│   │   │   │   │   ├── footer.component.html
│   │   │   │   │   └── footer.component.ts
│   │   │   │   ├── home.component.css
│   │   │   │   ├── home.component.html
│   │   │   │   └── home.component.ts
│   │   │   ├── licence/
│   │   │   │   ├── licence.component.css
│   │   │   │   ├── licence.component.html
│   │   │   │   └── licence.component.ts
│   │   │   ├── login/
│   │   │   │   ├── login.component.css
│   │   │   │   ├── login.component.html
│   │   │   │   └── login.component.ts
│   │   │   ├── ...
│   │   │   └── register/
│   │   │   │   ├── register.component.css
│   │   │   │   ├── register.component.html
│   │   │   │   └── register.component.ts
│   │   ├── app.component.css
│   │   ├── app.component.ts
│   │   ├── app.module.ts
│   │   ├── ...
│   │   └── app.routing.ts
│   ├── assets/
│   │   ├── ca-cities.json
│   │   ├── states.json
│   │   ├── countries.json
│   │   └── img/
│   │       ├── BanQ-2.gif
│   │       ├── logo_BAnQ.jpg
│   │       ├── banq-title.png
│   │       ├── 5_Guides-Form.jpg
│   │       └── ...
│   ├── environments/
│   │   ├── environment.prod.ts
│   │   └── environment.ts
│   ├── scss/
│   ├── index.html
│   └── ...
├── .angulardoc.json
├── angular.json
├── karma.conf.js
├── LICENSE
├── protractor.conf.js
├── README.md
├── tsconfig.json
├── tslint.json
├── package.json
└── ...
```

## Comments

- Package **app.core.data:** Contient les modeles de donnees persistants manipules dans l'application
- Package **app.core.enum:** Contient les types enumeres de l'application
- Package **app.core.models:** Contient les modeles de donnees non-persistants manipules dans l'application
- Package **app.core.services:** Contient les differentes couches de services de l'application
- Package **app.core.services.business:** Contient l'implementation des services metiers decrits dans l'interface
- Package **app.core.services.dao:** Contient les services d'acces aux donnees (couche d'acces aux webservices du backend)
- Package **app.core.services.interfaces:** Contient les interfaces qui decrivent l'ensemble des services metier a implementer
- Package **app.core.shared:** Contient les differents utilitaires et donnees statiques de l'application
- Package **app.modules:** Contient la declaration des differents modules de l'application, ainsi que le routage lie a chaque module
- Package **app.views:** Contient tous les composants visuels de l'application

## Documentation

The documentation for the idel-extranet-frontend is hosted at our website http://wiki.banq.qc.ca/confluence/pages/viewpage.action?pageId=4096057

## Contributing

Please read through our [contributing guidelines] opening issues, coding standards, and notes on development.
http://jira.banq.qc.ca/jira/secure/Dashboard.jspa


## Versioning

For transparency into our release cycle and in striving to maintain backward compatibility, idel-extranet-frontend is maintained under http://dev-idel.banq.qc.ca:8081/nexus/repository/maven-group/


## Creators

**BANQ - COFOMO**

* <http://www.banq.qc.ca/accueil/>
* <http://www.cofomo.com>


## Copyright and license

copyright 2019 BANQ.

## Support IDEL Development

idel-extranet-frontend is an BANQ licensed open source project and completely free to use. However, the amount of effort needed to maintain and develop new features for the project is not sustainable without proper financial backing.
